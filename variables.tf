variable resource_group_name {
  type = string
}

variable location {
  type = string
}

variable resource_identifier {
  type = string
}

variable account_kind {
  type    = string
  default = "StorageV2"
}

variable account_tier {
  type    = string
  default = "Standard"
}

variable access_tier {
  type    = string
  default = "Hot"
}

variable account_replication_type {
  type    = string
  default = "ZRS"
}

variable enable_https_traffic_only {
  type    = bool
  default = true
}

variable enable_advanced_threat_protection {
  type    = bool
  default = false
}

variable enable_hns {
  type    = bool
  default = false
}

variable network_rules_ip_rules {
  type    = list(string)
  default = []
}

variable network_rules_subnet_ids {
  type    = list(string)
  default = []
}

variable tags {
  description = "Tags to apply to all resources created."
  type        = map(string)
  default     = {}
}

variable containers {
  description = "List of containers to create and their access levels."
  type        = list(object({ name = string, access_type = string }))
  default     = []
}

variable fileshare_enabled {
  description = "Enable or disable fileshare"
  type        = bool
  default     = false
}

variable fileshare_quota {
  description = "(Optional) The maximum size of the share, in gigabytes. For Standard storage accounts, this must be greater than 0 and less than 5120 GB (5 TB). For Premium FileStorage storage accounts, this must be greater than 100 GB and less than 102400 GB (100 TB). Default is 5120."
  type        = number
  default     = null
}

variable fileshare_name {
  description = "(Required) The name of the share. Must be unique within the storage account where the share is located."
  type        = string
  default     = null
}
