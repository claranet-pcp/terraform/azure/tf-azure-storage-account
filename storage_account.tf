resource azurerm_storage_account "storage" {
  name                      = "st${local.resource_identifier}"
  resource_group_name       = var.resource_group_name
  location                  = var.location
  account_kind              = var.fileshare_enabled == true ? "FileStorage" : var.account_kind
  account_tier              = var.fileshare_enabled == true ? "Premium" : var.account_tier
  account_replication_type  = var.fileshare_enabled == true ? "ZRS" : var.account_replication_type
  access_tier               = var.access_tier
  enable_https_traffic_only = var.enable_https_traffic_only
  is_hns_enabled            = var.enable_hns

  dynamic "network_rules" {
    for_each = length(concat(var.network_rules_ip_rules, var.network_rules_subnet_ids)) > 0 ? ["true"] : []
    content {
      default_action             = "Allow"
      ip_rules                   = var.network_rules_ip_rules
      virtual_network_subnet_ids = var.network_rules_subnet_ids
    }
  }

  tags = var.tags
}

resource azurerm_advanced_threat_protection "storage" {
  target_resource_id = azurerm_storage_account.storage.id
  enabled            = var.enable_advanced_threat_protection
}
