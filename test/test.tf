terraform {
  required_version = "~> 0.14.3"
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.41.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource azurerm_resource_group "tf_azure_storage_account_test" {
  name     = "tfazstoractest"
  location = "uksouth"
}

module "simple" {
  source   = "../"
  resource_identifier = "testresident" # no more that 11 characters
  resource_group_name = azurerm_resource_group.tf_azure_storage_account_test.name
  location = "uksouth"
  containers = [{
    name = "appdata"
    access_type = "private"
  }]
  enable_https_traffic_only = false # default true
  enable_advanced_threat_protection = false # default false. not all regions are supported
}
