output "storage_account" {
  value = azurerm_storage_account.storage.*
}

output "container" {
  value = azurerm_storage_container.container.*
}
