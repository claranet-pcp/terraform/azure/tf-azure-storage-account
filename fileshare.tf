resource "azurerm_storage_share" "fileshare" {
  count                = var.fileshare_enabled == true ? 1 : 0
  name                 = var.fileshare_name
  storage_account_name = azurerm_storage_account.storage.name
  quota                = var.fileshare_quota
}
