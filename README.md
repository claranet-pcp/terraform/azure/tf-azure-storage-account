#<u>Azure Storage Account Module </u>
This module is to simplify the creation of Azure Storage Accounts. The majortity of the most frequently used settings have been made defaults.

## Usage

```terraform
module "simple" {
    source   = "git::https://gitlab.com/claranet-pcp/terraform/azure/tf-azure-storage-account.git?ref=v1.0.0"
    resource_identifier = "sqlbackups" # no more that 11 characters
    resource_group_name = module.resource_group.resource_group_name
    location = "westeurope"
    containers = [
        {
            name = "appdata"
            access_type = "private"
        },
    ]

    enable_https_traffic_only = false # default true
    enable_advanced_threat_protection = true # default false. not all regions are supported

    tags = local.standard_tags # map of tags

}
```

## wip

file share creation
table creation
